import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StudentsComponent} from "./students/students.component";
import {StudentDetailComponent} from "./students/student-detail/student-detail.component";
import {DisciplinesComponent} from "./disciplines/disciplines.component";
import {StudentNewComponent} from "./students/student-new/student-new.component";
import {ClientsComponent} from "./clients/clients.component";
import {ClientNewComponent} from "./clients/client-new/client-new.component";
import {ClientUpdateComponent} from "./clients/client-update/client-update.component";
import {ClientFilterComponent} from "./clients/client-filter/client-filter.component";

import {BooksComponent} from "./books/books.component";
import {BookNewComponent} from "./books/book-new/book-new.component";
import {BookUpdateComponent} from "./books/book-update/book-update.component";
import {BookFilterComponent} from "./books/book-filter/book-filter.component";

import {TransactionsComponent} from "./transactions/transactions.component";
import {TransactionNewComponent} from "./transactions/transaction-new/transaction-new.component";
import {TransactionUpdateComponent} from "./transactions/transaction-update/transaction-update.component";

const routes: Routes = [
  // { path: '', redirectTo: '/home', pathMatch: 'full' },
  {path: 'students', component: StudentsComponent},
  {path: 'student/detail/:id', component: StudentDetailComponent},
  {path: 'student/new', component: StudentNewComponent},

  {path: 'disciplines', component: DisciplinesComponent},

  {path: 'clients', component: ClientsComponent},
  {path: 'client/new', component: ClientNewComponent},
  {path: 'client/update/:cnp', component: ClientUpdateComponent},
  {path: 'clients/filtered', component: ClientFilterComponent},

  {path: 'books', component: BooksComponent},
  {path: 'book/new', component: BookNewComponent},
  {path: 'book/update/:bid', component: BookUpdateComponent},
  {path: 'books/filtered', component: BookFilterComponent},

  {path: "transactions", component: TransactionsComponent},
  {path: 'transaction/new', component: TransactionNewComponent},
  {path: 'transaction/update/:cnp/:bid', component: TransactionUpdateComponent},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
