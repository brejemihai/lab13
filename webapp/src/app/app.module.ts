import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {AppRoutingModule} from "./app-routing.module";
import {StudentDetailComponent} from "./students/student-detail/student-detail.component";
import {StudentsComponent} from "./students/students.component";
import {StudentListComponent} from "./students/student-list/student-list.component";
import {StudentService} from "./students/shared/student.service";
import { DisciplinesComponent } from './disciplines/disciplines.component';
import { DisciplineListComponent } from './disciplines/discipline-list/discipline-list.component';
import {DisciplineService} from "./disciplines/shared/discipline.service";
import { StudentNewComponent } from './students/student-new/student-new.component';

import { ClientService } from './clients/shared/client.service';
import { ClientsComponent } from './clients/clients.component';
import { ClientNewComponent} from './clients/client-new/client-new.component';
import { ClientListComponent} from './clients/client-list/client-list.component';
import { ClientUpdateComponent } from './clients/client-update/client-update.component';

import { BookService } from './books/shared/book.service';
import { BooksComponent } from './books/books.component';
import { BookNewComponent } from './books/book-new/book-new.component';
import { BookUpdateComponent } from './books/book-update/book-update.component';
import { BookListComponent } from './books/book-list/book-list.component';

import { TransactionService } from "./transactions/shared/transaction.service";
import { TransactionsComponent } from './transactions/transactions.component';
import { TransactionNewComponent } from './transactions/transaction-new/transaction-new.component';
import { TransactionUpdateComponent } from './transactions/transaction-update/transaction-update.component';
import { TransactionListComponent } from './transactions/transaction-list/transaction-list.component';
import { BookFilterComponent } from './books/book-filter/book-filter.component';
import { ClientFilterComponent } from './clients/client-filter/client-filter.component';

@NgModule({
  declarations: [
    AppComponent,
    StudentDetailComponent,
    StudentsComponent,
    StudentListComponent,
    DisciplinesComponent,
    DisciplineListComponent,
    StudentNewComponent,
    ClientsComponent,
    ClientNewComponent,
    ClientListComponent,
    ClientUpdateComponent,
    BooksComponent,
    BookNewComponent,
    BookUpdateComponent,
    BookListComponent,
    TransactionsComponent,
    TransactionNewComponent,
    TransactionUpdateComponent,
    TransactionListComponent,
    BookFilterComponent,
    ClientFilterComponent,



  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [StudentService, DisciplineService, ClientService, BookService, TransactionService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
