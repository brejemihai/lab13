import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  moduleId: module.id,
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent{

  constructor(private router: Router) { }

  addNewTransaction()
  {
    console.log("Add new transaction button clicked");

    this.router.navigate(["transaction/new"]);
  }
}
