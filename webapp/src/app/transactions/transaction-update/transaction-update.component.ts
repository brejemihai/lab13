import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ActivatedRoute} from "@angular/router";
import { Inject }  from '@angular/core';
import { DOCUMENT } from '@angular/common';
import {Location} from "@angular/common";
import {TransactionListComponent} from "../transaction-list/transaction-list.component";
import {TransactionService} from "../shared/transaction.service";

@Component({
  selector: 'app-transaction-update',
  templateUrl: './transaction-update.component.html',
  styleUrls: ['./transaction-update.component.css']
})
export class TransactionUpdateComponent implements OnInit {

  theCNP: string;
  theBID: string;

  constructor(private transactionService: TransactionService,
              private location: Location,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              @Inject(DOCUMENT) document ) {
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe( params => { this.theCNP = params['cnp']; this.theBID = params['bid'] } );
    document.getElementById('targetu').innerHTML = "Updating transaction from CNP: " + this.theCNP + " of BID: " + this.theBID;
  }

  updateTransaction(transactionDate: Date, price: number)
  {
    console.log("saving transaction", transactionDate, price);

    let clientID = +this.theCNP;
    let bookID = +this.theBID;
    this.transactionService.updateTransaction({
      clientID,
      bookID,
      transactionDate,
      price
    })
      .subscribe(transaction => console.log("updated transaction: ", transaction));

      setTimeout(() =>
      {
          this.router.navigate(['transactions']);
      },
      500);
  }
}
