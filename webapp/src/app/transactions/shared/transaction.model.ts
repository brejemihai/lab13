export class Transaction
{
  clientID: number;
  bookID: number;
  transactionDate: Date;
  price: number;
}
