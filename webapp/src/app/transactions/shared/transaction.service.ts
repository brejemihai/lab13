import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Transaction} from "./transaction.model";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";

@Injectable()
export class TransactionService {
  private serviceUrl = 'http://localhost:8080/api/manage';

  constructor(private httpClient: HttpClient) {
  }

  getTransactions(): Observable<Transaction[]> {
    return this.httpClient
      .get<Array<Transaction>>(this.serviceUrl);
  }

  getTransaction(cid: number, bid:number): Observable<Transaction> {
    return this.getTransactions()
      .pipe(
        map(transactions => transactions.find(trans => trans.clientID === cid && trans.bookID === bid))
      );
  }

  saveTransaction(transaction: Transaction): Observable<Transaction> {
    console.log("saveTransaction", transaction);

    const addUrl = `${this.serviceUrl}/client=${transaction.clientID}&book=${transaction.bookID}`;
    return this.httpClient
      .post<Transaction>(addUrl, transaction);
  }

  updateTransaction(transaction: Transaction): Observable<Transaction>
  {
    const updUrl = `${this.serviceUrl}/client=${transaction.clientID}&book=${transaction.bookID}`;
    return this.httpClient
      .put<Transaction>(updUrl, transaction);
  }

  deleteTransaction(cnp: number, bid: number): Observable<any>
  {
    const url = `${this.serviceUrl}/client=${cnp}&book=${bid}`;
    return this.httpClient
      .delete(url);
  }

}
