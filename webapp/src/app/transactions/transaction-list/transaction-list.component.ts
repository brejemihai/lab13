import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import { CommonModule } from "@angular/common";
import {Transaction} from "../shared/transaction.model";
import {TransactionService} from "../shared/transaction.service";

@Component({
  moduleId: module.id,
  selector: 'app-transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.css']
})
export class TransactionListComponent implements OnInit {

  errorMessage: string;
  transactions: Array<Transaction>;
  selectedTransaction: Transaction;

  constructor(private transactionService: TransactionService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.getTransactions();
  }

  getTransactions() {
    this.transactionService.getTransactions()
      .subscribe(
        transactions => this.transactions = transactions,
        error => this.errorMessage = <any>error
      );
  }

  onSelect(transaction: Transaction): void {
    this.selectedTransaction = transaction;
  }

  deleteTransaction(clientID: number, bookID: number)
  {
    console.log("Deleting transaction: ", clientID, bookID);

    this.transactionService.deleteTransaction(clientID, bookID)
      .subscribe(_ => {
        console.log("Transaction deleted");

        this.transactions = Object.values(this.transactions)
          .filter(s => s.clientID !== clientID && s.bookID !== bookID);

        this.getTransactions();
      });
    this.router.navigate(["transactions"]);
  }

  updateTransaction(clientid: number, bookid: number)
  {
    this.router.navigate(["transaction/update/:cnp/:bid", {cnp: clientid, bid: bookid}]);
  }

}
