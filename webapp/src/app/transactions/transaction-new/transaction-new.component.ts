import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {Location} from "@angular/common";
import {TransactionService} from "../shared/transaction.service";

@Component({
  selector: 'app-transaction-new',
  templateUrl: './transaction-new.component.html',
  styleUrls: ['./transaction-new.component.css']
})
export class TransactionNewComponent implements OnInit {

  constructor(private transactionService: TransactionService,
              private location: Location,
              private router: Router) {
  }

  ngOnInit(): void {
  }

  checkIfYearValid(year: Date): boolean
  {
    if(year > new Date())
    {
      return false;
    }
    return true;
  }

  saveTransaction(clientID: number, bookID: number, transactionDate: Date, price: number)
  {
    console.log("saving transaction", clientID, bookID, transactionDate, price);

    if(!this.checkIfYearValid(transactionDate))
    {
      alert("Transaction cannot happen in the future");
    }

    this.transactionService.saveTransaction({
      clientID,
      bookID,
      transactionDate,
      price
    })
      .subscribe(transaction => console.log("saved transaction: ", transaction));

      setTimeout(() =>
      {
          this.router.navigate(['/transactions']);
      },
      100);
    //this.location.back(); // ...
  }

}
