import {Component, OnInit} from '@angular/core';
import { CommonModule } from "@angular/common";
import {Client} from "../shared/client.model";
import {ClientService} from "../shared/client.service";
import {Router} from "@angular/router";
import {Observable} from "rxjs";


@Component({
  moduleId: module.id,
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css'],
})
export class ClientListComponent implements OnInit {
  errorMessage: string;
  clients: Array<Client>;
  selectedClient: Client;

  constructor(private clientService: ClientService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.getClients();
  }

  getClients() {
    this.clientService.getClients()
      .subscribe(
        clients => this.clients = clients,
        error => this.errorMessage = <any>error
      );
  }

  onSelect(client: Client): void {
    this.selectedClient = client;
  }

  deleteClient(client: Client)
  {
    console.log("Deleting client: ", client);

    this.clientService.deleteClient(client.cnp)
      .subscribe(_ => {
        console.log("Client deleted");

        this.clients = Object.values(this.clients)
          .filter(s => s.cnp !== client.cnp);

        this.getClients();
      });
    this.router.navigate(["clients"]);
  }

  updateClient(cnp: string)
  {
    this.router.navigate(["client/update", cnp]);
  }
}
