export class ClientSpent
{
  cnp: number;
  yob: Date;
  gender: string;
  price: number;
}
