import {Injectable} from '@angular/core';

import {HttpClient} from "@angular/common/http";

import {Client} from "./client.model";
import {ClientSpent} from "./client-spent.model";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";

@Injectable()
export class ClientService {
  private clientsUrl = 'http://localhost:8080/api/clients';

  constructor(private httpClient: HttpClient) {
  }

  getClients(): Observable<Client[]> {
    return this.httpClient
      .get<Array<Client>>(this.clientsUrl);
  }

  getStudent(id: number): Observable<Client> {
    return this.getClients()
      .pipe(
        map(clients => clients.find(client => client.cnp === id))
      );
  }

  saveClient(client: Client): Observable<Client> {
    console.log("saveClient", client);

    return this.httpClient
      .post<Client>(this.clientsUrl, client);
  }

  updateClient(client: Client): Observable<Client>
  {
    return this.httpClient
      .put<Client>(this.clientsUrl, client);
  }

  deleteClient(cnp: number): Observable<any>
  {
    const url = `${this.clientsUrl}/${cnp}`;
    return this.httpClient
      .delete(url);
  }

  getSortedClients(): Observable<ClientSpent[]>
  {
    return this.httpClient
      .get<Array<ClientSpent>>(`${this.clientsUrl}/spent`);
  }

}
