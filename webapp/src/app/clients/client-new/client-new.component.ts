import {Component, OnInit} from '@angular/core';
import {ClientService} from "../shared/client.service";
import {Location} from "@angular/common";
import {ClientListComponent} from "../client-list/client-list.component";
import {Router} from "@angular/router";

@Component({
  selector: 'app-client-new',
  templateUrl: './client-new.component.html',
  styleUrls: ['./client-new.component.css']
})
export class ClientNewComponent implements OnInit {

  constructor(private clientService: ClientService,
              private location: Location,
              private router: Router) {
  }

  ngOnInit(): void {
  }


  saveClient(cnp: number, yob: Date, gender: string)
  {
    console.log("saving client", cnp, yob, gender);

    this.clientService.saveClient({
      cnp,
      yob,
      gender
    })
      .subscribe(client => console.log("saved client: ", client));

    this.router.navigate(["/clients"]);
    //this.location.back(); // ...
  }
}
