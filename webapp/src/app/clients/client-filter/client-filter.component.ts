import { Component, OnInit } from '@angular/core';
import { CommonModule } from "@angular/common";
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {ClientSpent} from "../shared/client-spent.model";
import {ClientService} from "../shared/client.service";


@Component({
    moduleId: module.id,
  selector: 'app-client-filter',
  templateUrl: './client-filter.component.html',
  styleUrls: ['./client-filter.component.css']
})
export class ClientFilterComponent implements OnInit {

  errorMessage: string;
  clients: Array<ClientSpent>;

  constructor(private clientService: ClientService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.getSortedClients();
  }

  getSortedClients() {
    this.clientService.getSortedClients()
      .subscribe(
        clients => this.clients = clients,
        error => this.errorMessage = <any>error
      );
  }

}
