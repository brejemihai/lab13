import { Component, OnInit } from '@angular/core';
import {ClientService} from "./shared/client.service";
import {Router} from "@angular/router";

@Component({
  moduleId: module.id,
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent {

  constructor(private router: Router) { }

  addNewClient()
  {
    console.log("Add new client button clicked");

    this.router.navigate(["client/new"]);
  }

  showFilteredClients()
  {
    this.router.navigate(["clients/filtered"]);
  }

}
