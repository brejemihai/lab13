import {Component, OnInit} from '@angular/core';
import {ClientService} from "../shared/client.service";
import {Location} from "@angular/common";
import {ClientListComponent} from "../client-list/client-list.component";
import {Router} from "@angular/router";
import {ActivatedRoute} from "@angular/router";
import { Inject }  from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-client-update',
  templateUrl: './client-update.component.html',
  styleUrls: ['./client-update.component.css']
})
export class ClientUpdateComponent implements OnInit {

  theCNP: string;

  constructor(private clientService: ClientService,
              private location: Location,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              @Inject(DOCUMENT) document ) {
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe( params => { this.theCNP = params['cnp']; } );
    document.getElementById('targetu').innerHTML = "Updating client with CNP: " + this.theCNP;
  }

  updateClient(yob: Date, gender: string)
  {
    console.log("saving client", yob, gender);

    let cnp = +this.theCNP;
    this.clientService.updateClient({
      cnp,
      yob,
      gender
    })
      .subscribe(client => console.log("updated client: ", client));

      setTimeout(() =>
      {
          this.router.navigate(['clients']);
      },
      100);
  }

}
