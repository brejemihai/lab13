import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Book} from "./book.model";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";

@Injectable()
export class BookService {
  private serviceUrl = 'http://localhost:8080/api/books';

  constructor(private httpClient: HttpClient) {
  }

  getBooks(): Observable<Book[]> {
    return this.httpClient
      .get<Array<Book>>(this.serviceUrl);
  }

  getBook(id: number): Observable<Book> {
    return this.getBooks()
      .pipe(
        map(book => book.find(book => book.BID === id))
      );
  }

  saveBook(book: Book): Observable<Book> {
    console.log("saveBook", book);

    return this.httpClient
      .post<Book>(this.serviceUrl, book);
  }

  updateBook(book: Book): Observable<Book>
  {
    return this.httpClient
      .put<Book>(this.serviceUrl, book);
  }

  deleteBook(bid: number): Observable<any>
  {
    const url = `${this.serviceUrl}/${bid}`;
    return this.httpClient
      .delete(url);
  }

}
