import { Component, OnInit } from '@angular/core';
import {BookService} from "./shared/book.service";
import {Router} from "@angular/router";

@Component({
  moduleId: module.id,
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent{

  constructor(private router: Router) { }

  addNewBook()
  {
    console.log("Add new book button clicked");

    this.router.navigate(['book/new']);
  }

  filterBooks()
  {
    this.router.navigate(['books/filtered']);
  }


}
