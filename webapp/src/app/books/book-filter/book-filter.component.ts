import { Component, OnInit } from '@angular/core';
import { CommonModule } from "@angular/common";
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {Book} from '../shared/book.model';
import {BookService} from '../shared/book.service';

@Component({
  moduleId: module.id,
  selector: 'app-book-filter',
  templateUrl: './book-filter.component.html',
  styleUrls: ['./book-filter.component.css']
})
export class BookFilterComponent implements OnInit {

  errorMessage: string;
  books: Array<Book>;
  filtered: Array<Book>;
  selectedBook: Book;

  constructor(private bookService: BookService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.getBooks();
    this.filtreaza("");
  }

  getBooks() {
    this.bookService.getBooks()
      .subscribe(
        books => this.books = books,
        error => this.errorMessage = <any>error
      );
  }

  filtreaza(numele: string) {
      this.getBooks();
      if (numele.trim() !== "")
      {
        this.filtered = this.books['books'].filter(s => s.author.includes(numele));
      }
      else
      {
        this.filtered = this.books['books'];
      }

  }

  onSelect(book: Book): void {
    this.selectedBook = book;
  }

  deleteBook(book: Book)
  {
    console.log("Deleting book: ", book);

    this.bookService.deleteBook(book.BID)
      .subscribe(_ => {
        console.log("Book deleted");

        this.books = Object.values(this.books)
          .filter(s => s.BID !== book.BID);

        this.getBooks();
      });
    this.router.navigate(["books"]);
  }

  updateBook(BID: string)
  {
    this.router.navigate(["book/update", BID]);
  }


}
