import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ActivatedRoute} from "@angular/router";
import { Inject }  from '@angular/core';
import { DOCUMENT } from '@angular/common';
import {Location} from "@angular/common";
import {BookService} from '../shared/book.service';
import {BookListComponent} from '../book-list/book-list.component';

@Component({
  selector: 'app-book-update',
  templateUrl: './book-update.component.html',
  styleUrls: ['./book-update.component.css']
})
export class BookUpdateComponent implements OnInit {

  theBID: string;

  constructor(private bookService: BookService,
              private location: Location,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              @Inject(DOCUMENT) document ) {
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe( params => { this.theBID = params['bid']; } );
    document.getElementById('targetu').innerHTML = "Updating book with BID: " + this.theBID;
  }

  checkIfYearValid(year: number): boolean
  {
    if(+year > new Date().getFullYear())
    {
      return false;
    }
    return true;
  }

  updateBook(author: string, year: number)
  {
    console.log("saving book", author, year);

    if(!this.checkIfYearValid(year))
    {
      alert("Book year must not be in the future");
    }

    let BID = +this.theBID;
    this.bookService.updateBook({
      BID,
      author,
      year
    })
      .subscribe(book => console.log("updated book: ", book));

      setTimeout(() =>
      {
          this.router.navigate(['books']);
      },
      100);
  }

}
