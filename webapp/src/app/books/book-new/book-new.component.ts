import { Component, OnInit } from '@angular/core';
import {BookService} from "../shared/book.service";
import {Location} from "@angular/common";
import {BookListComponent} from "../book-list/book-list.component";
import {Router} from "@angular/router";

@Component({
  selector: 'app-book-new',
  templateUrl: './book-new.component.html',
  styleUrls: ['./book-new.component.css']
})
export class BookNewComponent implements OnInit {

  constructor(private bookService: BookService,
              private location: Location,
              private router: Router) {
  }

  ngOnInit(): void {
  }

  checkIfYearValid(year: number): boolean
  {
    if(+year > new Date().getFullYear())
    {
      return false;
    }
    return true;
  }

  saveBook(BID: number, author: string, year: number)
  {
    console.log("saving book", BID, author, year);

    if(!this.checkIfYearValid(year))
    {
      alert("Book year must not be in the future");
    }
    
    this.bookService.saveBook({
      BID,
      author,
      year
    })
      .subscribe(book => console.log("saved book: ", book));

      setTimeout(() =>
      {
          this.router.navigate(['books']);
      },
      100);
  }

}
