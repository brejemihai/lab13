//package breje.core.service;
//
//import breje.core.model.Book;
//import breje.core.model.Client;
//import breje.core.model.Transaction;
//import breje.core.model.validators.TransactionValidator;
//import breje.core.repository.BookRepository;
//import breje.core.repository.ClientRepository;
//import breje.core.repository.TransactionRepository;
//import breje.core.repository.TransactionRepositoryImpl;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.*;
//import java.util.stream.Collectors;
//import java.util.stream.StreamSupport;
//
//@Service
//public class TransactionService {
//
//    private static final Logger log = LoggerFactory.getLogger(TransactionService.class);
//
//    @Autowired
//    private ClientRepository clientRepository;
//
//    @Autowired
//    private BookRepository bookRepository;
//
//    @Autowired
//    private ClientService clientService;
//
//    @Autowired
//    private BookService bookService;
//
//    public TransactionService()
//    {
//        log.trace("TransactionService initialized");
//    }
//
////    public void addTransaction(Transaction t) throws Exception
////    {
////        if (t.getID() == null)
////        {
////            t.setID(t.getBookID() + t.getClientID());
////        }
////        transactionRepository.addTransaction(t);
////    }
////
////    public void deleteTransaction(Long cnp, Long bid) throws Exception
////    {
////        this.transactionRepository.deleteTransaction(cnp,bid);
////    }
////
////    public void updateTransaction(Transaction t) throws Exception
////    {
////        if (t.getID() == null)
////        {
////            t.setID(t.getBookID() + t.getClientID());
////        }
////        if (t.getTransactionDate() == null)
////        {
////            if(checkTransactions(t.getID()))
////            {
////                Transaction old = this.transactionRepository.getTransactionByTID(t.getID()).get();
////                t.setTransactionDate(old.getTransactionDate());
////            }
////        }
////
////        if (t.getPrice() == 0)
////        {
////            if(checkTransactions(t.getID()))
////            {
////                Transaction old = this.transactionRepository.getTransactionByTID(t.getID()).get();
////                t.setPrice(old.getPrice());
////            }
////        }
////        this.transactionRepository.updateTransaction(t);
////    }
////
////    public Optional<Transaction> getTransactionByTID(Long tid) throws Exception
////    {
////        return transactionRepository.getTransactionByTID(tid);
////    }
////
////    public boolean checkTransactions(Long tid) throws Exception
////    {
////        return transactionRepository.checkTransactions(tid);
////    }
////
////    public Iterable<Transaction> getAllTransactions()
////    {
////        return transactionRepository.getAllTransactions();
////    }
////
////    public Iterable<Transaction> getAllTransactionForClient(Long cid) throws Exception
////    {
////        return this.transactionRepository.getAllTransactionsForClient(cid);
////    }
////
////    public Iterable<Transaction> getAllTransactionForBook(Long bid) throws Exception
////    {
////        return this.transactionRepository.getAllTransactionsForBook(bid);
////    }
////
////    public Iterable<Transaction> getAllTransactionWithPrice(int price)
////    {
////        return this.transactionRepository.findByPrice(price);
////    }
////    public List<Map.Entry<Client, Integer>> reportClientsBySpending() throws Exception {
////        Iterable<Client> clientsInSystem = this.clientService.getAllClients();
////        Iterable<Transaction> allTransactions = this.getAllTransactions();
////
////        Map<Client, Integer> clientPrice = new HashMap<>();
////        clientsInSystem.forEach(x->clientPrice.put(x, 1));
////        allTransactions.forEach(
////                x ->
////                {
////                    if (x != null) {
////                        Client cl = null;
////
////                        Book bk = null;
////                        if (this.clientService.checkClient(x.getClientID())) {
////                            cl = (Client) this.clientService.getOneClient(x.getClientID()).get();
////                            Integer oldPrice = clientPrice.get(cl);
////                            if (oldPrice != null)
////                            {
////                                if (this.bookService.checkBook(x.getBookID())) {
////                                    bk = (Book) this.bookService.getOneBook(x.getBookID()).get();
////                                    Integer newPrice = oldPrice + Integer.valueOf(x.getPrice());
////                                    clientPrice.put(cl, newPrice);
////                                }
////                            }
////                        }
////                    }
////                }
////        );
////
////        Set<Map.Entry<Client, Integer>> set = clientPrice.entrySet();
////        List<Map.Entry<Client, Integer>> list = new ArrayList<Map.Entry<Client, Integer>>(set);
////        list.sort(new Comparator<Map.Entry<Client, Integer>>() {
////            public int compare(Map.Entry<Client, Integer> o1, Map.Entry<Client, Integer> o2) {
////                int result = (o1.getValue()).compareTo(o2.getValue());
////                if (result != 0) {
////                    return result;
////                }
////                else {
////                    if (o1.getKey().getID() < o2.getKey().getID()) {
////                        return 1;
////                    }
////                    else {
////                        return 0;
////                    }
////                }
////            }
////        });
////        return list;
////    }
////
////    public Set<Transaction> reportSellsBefore(Date beforeDate)
////    {
////        return StreamSupport.stream(this.getAllTransactions().spliterator(), false)
////                .filter(
////                        x ->
////                        {
////                            return x.getTransactionDate().before(beforeDate);
////                        }
////                )
////                .collect(Collectors.toSet());
////    }
//}
