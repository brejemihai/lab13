package breje.core.service;

import breje.core.exceptions.BaseException;
import breje.core.model.Book;
import breje.core.model.Client;
import breje.core.model.Transaction;
import breje.core.model.validators.ClientValidator;
import breje.core.repository.BookRepository;
import breje.core.repository.ClientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.*;

@Service
public class ClientService {

    private static final Logger log = LoggerFactory.getLogger(ClientService.class);

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private BookRepository bookRepository;

    public ClientService()
    {
        log.trace("ClientService initialized");
    }


    public void addClient(Client client) throws Exception
    {
        if(client.getID() == null)
        {
            client.setID(client.getCNP());
        }
        clientRepository.save(client);
    }

    public void updateClient(Client client) throws Exception
    {
        if(client.getID() == null)
        {
            client.setID(client.getCNP());
        }

        if(client.getGender() == null || client.getGender().strip().equals(""))
        {
            if(checkClient(client.getID()))
            {
                Client old = this.getOneClient(client.getID()).get();
                client.setGender(old.getGender());
            }
        }
        if(client.getYob() == null)
        {
            if(checkClient(client.getID()))
            {
                Client old = this.getOneClient(client.getID()).get();
                client.setYob(old.getYob());
            }
        }
        clientRepository.save(client);
    }

    public void deleteClient(Long clientID) throws Exception
    {
        if(clientRepository.findById(clientID).isPresent())
        {
            clientRepository.delete(clientRepository.findById(clientID).get());
        }


    }

    @Transactional
    public void addTransaction(long client, long book, int price, Date transactionDate)
    {
        if(this.clientRepository.findById(client).isPresent() && this.bookRepository.findById(book).isPresent())
        {
            this.clientRepository.findById(client).get().addTransaction(this.bookRepository.findById(book).get(), price, transactionDate);
        }
        else
        {
            throw new BaseException("No such entitites");
        }
    }

    public void removeTransaction(Client client, Book book)
    {
        client.removeTransaction(client, book);
    }

    public void updateTransaction(long client, long book, int price, Date transactionDate)
    {
        if(this.clientRepository.findById(client).isPresent() && this.bookRepository.findById(book).isPresent())
        {
            this.clientRepository.findById(client).get().updateTransaction(this.bookRepository.findById(book).get(), price, transactionDate);
        }
        else
        {
            throw new BaseException("No such entitites");
        }
    }

    public Iterable<Transaction> getAllTransactionsForClient(Long cid)
    {
        if(this.checkClient(cid))
        {
            return this.getOneClient(cid).get().getTransactions();
        }
        return new HashSet<>();
    }

    public Iterable<Transaction> getAllTransactionWithPrice(int price)
    {
        Iterable<Client> clienti = this.clientRepository.findAllWithTransactionsAndBook();
        HashSet<Transaction> aleaBune = new HashSet<>();
        for (Client client : clienti) {
            client.getTransactions().stream().filter(x -> x.getPrice() == price).forEach(aleaBune::add);
        }
        return aleaBune;
    }

    public Optional<Client> getOneClient(Long clientID)
    {
        return clientRepository.findById(clientID);
    }

    public boolean checkClient(Long cnp)
    {
        return clientRepository.findById(cnp).isPresent();
    }

    public Iterable<Client> getAllClients()
    {
        return clientRepository.findAllWithTransactionsAndBook();
    }

//    public List<Map.Entry<Client, Integer>> reportClientsBySpending() throws Exception
//    {
//        Iterable<Client> clientsInSystem = this.getAllClients();
//
//        Set<Transaction> allTransactions = new HashSet<>();
//
//        clientsInSystem.forEach(
//                x ->
//                {
//                    x.getBooks().forEach(allTransactions::add);
//                }
//        );
//
//        Map<Client, Integer> clientPrice = new HashMap<>();
//        clientsInSystem.forEach(x->clientPrice.put(x, 1));
//        allTransactions.forEach(
//                x ->
//                {
//                    if (x != null) {
//                        Client cl = null;
//
//                        Book bk = null;
//                        if (this.checkClient(x.getClient().getID())) {
//                            cl = (Client) this.getOneClient(x.getClient().getID()).get();
//                            Integer oldPrice = clientPrice.get(cl);
//                            if (oldPrice != null)
//                            {
//                                if (this.bookService.checkBook(x.getBook().getID())) {
//                                    bk = (Book) this.bookService.getOneBook(x.getBook().getID()).get();
//                                    Integer newPrice = oldPrice + Integer.valueOf(x.getPrice());
//                                    clientPrice.put(cl, newPrice);
//                                }
//                            }
//                        }
//                    }
//                }
//        );
//
//        Set<Map.Entry<Client, Integer>> set = clientPrice.entrySet();
//        List<Map.Entry<Client, Integer>> list = new ArrayList<Map.Entry<Client, Integer>>(set);
//        list.sort(new Comparator<Map.Entry<Client, Integer>>() {
//            public int compare(Map.Entry<Client, Integer> o1, Map.Entry<Client, Integer> o2) {
//                int result = (o1.getValue()).compareTo(o2.getValue());
//                if (result != 0) {
//                    return result;
//                }
//                else {
//                    if (o1.getKey().getID() < o2.getKey().getID()) {
//                        return 1;
//                    }
//                    else {
//                        return 0;
//                    }
//                }
//            }
//        });
//        return list;
//    }
}
