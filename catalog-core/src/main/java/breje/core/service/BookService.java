package breje.core.service;

import breje.core.model.Book;
import breje.core.model.Client;
import breje.core.model.Transaction;
import breje.core.model.validators.BookValidator;
import breje.core.model.validators.Validator;
import breje.core.repository.BookRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class BookService
{
    private static final Logger log = LoggerFactory.getLogger(BookService.class);

    @Autowired
    private BookRepository bookRepository;

    public BookService()
    {
        log.trace("Book service initialized");
    }


    public void addBook(Book book) throws Exception
    {
        log.trace("addBook method called: Book {}", book);
        if(book.getID() == null)
        {
            book.setID(book.getBID());
        }
        bookRepository.save(book);
        log.trace("addBook method completed");
    }

    public void addTransaction(Book book, Client client, int price, Date transactionDate)
    {
        book.addTransaction(client, price, transactionDate);
    }

    public Iterable<Transaction> getAllTransactionsForBook(Long bid)
    {
        if(this.checkBook(bid))
        {
            return this.getOneBook(bid).get().getTransactions();
        }
        return new HashSet<>();
    }

    public void updateBook(Book book) throws Exception
    {
        log.trace("updateBook method called: Book {}", book);
        if(book.getID() == null)
        {
            book.setID(book.getBID());
        }
        if(book.getAuthor() == null || book.getAuthor().strip().equals(""))
        {
            if(checkBook(book.getBID()))
            {
                Book old = this.getOneBook(book.getBID()).get();
                book.setAuthor(old.getAuthor());
            }
        }
        if(book.getYear() == 0)
        {
            if(checkBook(book.getBID()))
            {
                Book old = this.getOneBook(book.getBID()).get();
                book.setYear(old.getYear());
            }
        }
        bookRepository.save(book);
        log.trace("updateBook method completed");
    }

    public void deleteBook(Long bookID) throws Exception
    {
        log.trace("deleteBook method called: ID {}", bookID);
        if (bookRepository.findById(bookID).isPresent())
        {
            bookRepository.delete(bookRepository.findById(bookID).get());
        }
        log.trace("deleteBook method completed");
    }

    public Optional<Book> getOneBook(Long bookID)
    {
        return bookRepository.findById(bookID);
    }


    public boolean checkBook(Long bid)
    {
        return bookRepository.findById(bid).isPresent();
    }

    public Iterable<Book> getAllBooks()
    {
        return bookRepository.findAllWithTransactionsAndClient();
    }

    public Iterable<Book> getBookContainingSubstring(String substr)
    {
        Iterable<Book> all = this.bookRepository.findAllWithTransactionsAndClient();
        Iterable<Book> filtrat = StreamSupport.stream(all.spliterator(), false).filter(x -> x.getAuthor().contains(substr)).collect(Collectors.toList());
        return filtrat;
    }

}
