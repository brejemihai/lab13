package breje.core.model;

import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@Table(name="client")
@NamedEntityGraphs(
        {
                @NamedEntityGraph(name = "clientWithTransaction",
                        attributeNodes = @NamedAttributeNode(value = "clientTransactions")),

                @NamedEntityGraph(name = "clientWithTransactionAndBook",
                        attributeNodes = @NamedAttributeNode(value = "clientTransactions",
                                subgraph = "transactionWithBook"),
                        subgraphs = @NamedSubgraph(name = "transactionWithBook",
                                attributeNodes = @NamedAttributeNode(value = "book")))
        }
)
public class Client extends BaseEntity<Long> implements Serializable
{
    @Column(name="cnp")
    @NotNull
    private long CNP;

    @NotNull
    private Date yob;

    @NotBlank
    private String gender;

    @OneToMany(mappedBy = "client", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Transaction> clientTransactions = new ArrayList<>();


    public Set<Book> getBooks()
    {
        clientTransactions = clientTransactions == null ? new ArrayList<>() : clientTransactions;
        return this.clientTransactions.stream().map(Transaction::getBook).collect(Collectors.toUnmodifiableSet());
    }

    public List<Transaction> getTransactions()
    {
        return this.clientTransactions;
    }

    public void addBook(Book book)
    {
        Transaction trans = new Transaction();
        trans.setBook(book);;
        trans.setClient(this);
        clientTransactions.add(trans);
    }

    public void addBooks(Set<Book> books)
    {
        books.forEach(this::addBook);
    }

    @Transactional
    public void addTransaction(Book book, int price, Date transactionData)
    {
        Transaction trans = new Transaction();
        trans.setBook(book);
        trans.setClient(this);
        trans.setPrice(price);
        trans.setTransactionDate(transactionData);
        clientTransactions.add(trans);
    }

    @Transactional
    public void updateTransaction(Book book, int price, Date transactionDate)
    {
        clientTransactions = clientTransactions.stream().filter(x -> x.getClient() != this && x.getBook() != book).collect(Collectors.toList());
        Transaction trans = new Transaction();
        trans.setBook(book);
        trans.setClient(this);
        trans.setPrice(price);
        trans.setTransactionDate(transactionDate);
        clientTransactions.add(trans);
    }

    @Transactional
    public void removeTransaction(Client client, Book book)
    {
        clientTransactions = clientTransactions.stream().filter(x -> x.getClient() != client && x.getBook() != book).collect(Collectors.toList());
    }

    public Client()
    {
        this.CNP = 0;
        this.yob = null;
        this.gender = null;
    }

    public Client(long CNP, Date yob, String gender)
    {
        this.CNP = CNP;
        this.yob = yob;
        this.gender = gender;
    }

    //getters
    public long getCNP()
    {
        return this.CNP;
    }

    public Date getYob()
    {
        return this.yob;
    }

    public String getGender()
    {
        return this.gender;
    }

    //setters
    public void setCNP(long newCNP)
    {
        this.CNP = newCNP;
    }

    public void setYob(Date newYob)
    {
        this.yob = newYob;
    }

    public void setGender(String newGender)
    {
        this.gender = newGender;
    }

    //verify
    public boolean equals(Object obj)
    {
        if( obj instanceof Client)
        {
            return ((Client) obj).getCNP() == CNP;
        }
        return false;
    }

    //toString
    public String toString()
    {
        return "Client { " + "ID: " +  String.valueOf(CNP) + " Year of birth: " + yob.toString() + " Gender: " + gender + " } " + super.toString();
    }

}
