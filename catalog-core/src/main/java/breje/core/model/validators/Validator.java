package breje.core.model.validators;

import breje.core.exceptions.ValidatorException;

/**
 * Interface for generic validations
 *
 * @author breje.
 */
public interface Validator<T>
{
    void validate(T entity) throws ValidatorException;
}