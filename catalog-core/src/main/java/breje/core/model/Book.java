package breje.core.model;



import jdk.jfr.Name;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@Table(name="book")
@NamedEntityGraphs(
        {
                @NamedEntityGraph(name = "bookWithTransaction",
                attributeNodes = @NamedAttributeNode(value = "bookTransactions")),

                @NamedEntityGraph(name = "bookWithTransactionAndClient",
                attributeNodes = @NamedAttributeNode(value = "bookTransactions",
                                        subgraph = "transactionWithClient"),
                subgraphs = @NamedSubgraph(name = "transactionWithClient",
                attributeNodes = @NamedAttributeNode(value = "client")))
        }
)
public class Book extends BaseEntity<Long> implements Serializable
{
    @Column(name="bid")
    @NotNull
    private long BID;

    @NotBlank
    private String author;

    @NotNull
    private int year;

    public Book()
    {
        this.BID = 0;
        this.author = null;
        this.year = 0;
    }
    public Book(long BID, String author, int year)
    {
        this.BID = BID;
        this.author = author;
        this.year = year;
    }

    @OneToMany(mappedBy = "book", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Transaction> bookTransactions = new ArrayList<>();

    public List<Client> getClients()
    {
        return this.bookTransactions.stream().map(Transaction::getClient).collect(Collectors.toUnmodifiableList());
    }

    public List<Transaction> getTransactions()
    {
        return this.bookTransactions;
    }


    public void addClient(Client client)
    {
        Transaction transaction = new Transaction();
        transaction.setClient(client);
        transaction.setBook(this);
        bookTransactions.add(transaction);
    }

    public void addTransaction(Client client, int price, Date transactionData)
    {
        Transaction trans = new Transaction();
        trans.setBook(this);
        trans.setClient(client);
        trans.setPrice(price);
        trans.setTransactionDate(transactionData);
        bookTransactions.add(trans);
    }

    public long getBID()
    {
        return BID;
    }

    public void setBID(long BID)
    {
        this.BID = BID;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public int getYear()
    {
        return year;
    }

    public void setYear(int year)
    {
        this.year = year;
    }

    @Override
    public String toString()
    {
        return "Book: " +
                " BID=" + BID +
                ", author=" + author  +
                ", year=" + year;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof Book)
            if(((Book) obj).getBID()==BID && ((Book) obj).getAuthor()==author && ((Book) obj).getYear()==year)
                return true;
        return false;
    }
}
