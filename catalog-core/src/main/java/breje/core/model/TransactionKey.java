package breje.core.model;

import java.io.Serializable;
import java.util.Objects;

public class TransactionKey implements Serializable
{
    private Client client;
    private Book book;

    public TransactionKey() {
    }

    public TransactionKey(Client client, Book book) {
        this.client = client;
        this.book = book;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionKey that = (TransactionKey) o;
        return Objects.equals(client, that.client) &&
                Objects.equals(book, that.book);
    }

    @Override
    public int hashCode() {
        return Objects.hash(client, book);
    }

    @Override
    public String toString() {
        return "TransactionKey{" +
                "client=" + client +
                ", book=" + book +
                '}';
    }
}
