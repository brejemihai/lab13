package breje.core.model;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;


@Entity
@Table(name="transaction")
@IdClass(TransactionKey.class)
public class Transaction implements Serializable
{
    @Id
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "book__id")
    private Book book;

    @Id
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "client__id")
    private Client client;

    public Transaction(Book book, Client client, @NotNull Date transactionDate, @NotNull int price) {
        this.book = book;
        this.client = client;
        this.transactionDate = transactionDate;
        this.price = price;
    }

    private Date transactionDate;

    private int price;

    public Transaction()
    {
        this.book = null;
        this.client = null;
        this.transactionDate = null;
        this.price = 0;
    }

    public Book getBook() {
        return book;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "book=" + book +
                ", client=" + client +
                ", transactionDate=" + transactionDate +
                ", price=" + price +
                '}';
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Client getClient()
    {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return Objects.equals(book, that.book) &&
                Objects.equals(client, that.client) &&
                Objects.equals(transactionDate, that.transactionDate);
    }

}
