package breje.core.model;


import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Optional;

@MappedSuperclass
@Table(name="baseentity")
public class BaseEntity<ID> implements Serializable
{
    @Id
    @Column(name="_id")
    @NotNull
    private ID id;

    public ID getID()
    {
        return this.id;
    }

    public void setID(ID newID)
    {
        this.id = newID;
    }

    @Override
    public String toString() {
        return "BaseEntity {" + " id=" + id + " }";
    }


/*
    public BaseEntity<ID> createFromXMLElement(Element xml)
    {
        return new BaseEntity<>();
    }*/
}
