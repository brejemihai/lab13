package breje.core.repository;

import breje.core.model.Book;
import breje.core.model.Client;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.jpa.HibernateEntityManager;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class ClientRepositoryCustomImpl extends CustomRepositorySupport
{
    public List<Client> findAllWithTransactionsAndBookJPQL()
    {
        EntityManager entityManager = getEntityManager();
        Query query = entityManager.createQuery(
                "select distinct b from Client b left join fetch b.clientTransactions t left join t.book"
        );
        List<Client> clients = query.getResultList();

        return clients;
    }

//    public List<Client> findAllWithTransactionsAndBookCriteriaAPI()
//    {
//        EntityManager entityManager = getEntityManager();
//        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
//
//        CriteriaQuery<Client> query = criteriaBuilder.createQuery(Client.class);
//        query.distinct(Boolean.TRUE);
//        Root<Client> root = query.from(Client.class);
//
//        Fetch<Client, Transaction> bookTransactionFetch = root.fetch(Client_.bookTransactions, JoinType.LEFT);
//        bookTransactionFetch.fetch(Transaction_.client, JoinType.LEFT);
//
//        List<Client> clients = entityManager.createQuery(query).getResultList();
//        )
//    }

    @Transactional
    public List<Client> findAllWithTransactionsAndBookSQL()
    {
        HibernateEntityManager hibernateEntityManager = getEntityManager().unwrap(HibernateEntityManager.class);
        Session session = hibernateEntityManager.getSession();

        org.hibernate.Query query = session.createSQLQuery("select distinct {b.*},{t.*},{c.*} from client b " +
                "left join transaction t on a._id=t.client__id " +
                "left join book c on t.book__id=c._id ")
                .addEntity("b", Client.class)
                .addJoin("t", "b.clientTransactions")
                .addJoin("c", "t.book")
                .addEntity("b", Client.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        List<Client> clients = query.getResultList();
        return clients;
    }

    @Transactional
    public List<Book> findAllByGenderJQPL(String gender)
    {
        EntityManager entityManager = getEntityManager();
        Query query = entityManager.createQuery(
                "select distinct b from Client b left join fetch b.clientTransactions t left join t.client where b.gender= :pgender"
        );
        query.setParameter("pgender", gender);
        List<Book> books = query.getResultList();

        return books;
    }

//    @Transactional
//    public List<Book> findAllByGenderCriteriaAPI(String gender)
//    {
//        EntityManager entityManager = getEntityManager();
//        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
//
//        CriteriaQuery<Client> query = criteriaBuilder.createQuery(Client.class);
//        query.distinct(Boolean.TRUE);
//        Root<Client> root = query.from(Client.class);
//        query.where(criteriaBuilder.equals(root.get("gender"), gender));
//
//        Fetch<Client, Transaction> clientTransactionFetch = root.fetch(Client_.clientTransactions, JoinType.LEFT);
//        clientTransactionFetch.fetch(Transaction_.book, JoinType.LEFT);
//
//
//        List<Client> clients = entityManager.createQuery(query).getResultList();
//        return clients;
//    }


    @Transactional
    public List<Book> findAllByGenderSQL(String gender)
    {
        HibernateEntityManager hibernateEntityManager = getEntityManager().unwrap(HibernateEntityManager.class);
        Session session = hibernateEntityManager.getSession();

        org.hibernate.Query query = session.createSQLQuery("select distinct {b.*},{t.*},{c.*} from client b " +
                "left join transaction t on a._id=t.client__id " +
                "left join book c on t.book__id=c._id " +
                "where b.gender = :paramGender")
                .setParameter(":paramGender", gender)
                .addEntity("b", Client.class)
                .addJoin("t", "b.clientTransactions")
                .addJoin("c", "t.book")
                .addEntity("b", Client.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        List<Book> books = query.getResultList();
        return books;
    }
}
