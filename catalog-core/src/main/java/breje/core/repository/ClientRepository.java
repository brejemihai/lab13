package breje.core.repository;


import breje.core.model.Book;
import breje.core.model.Client;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientRepository extends SystemRepository<Client, Long>
{
    @Query("select distinct c from Client c")
    @EntityGraph(value = "clientWithTransactions", type = EntityGraph.EntityGraphType.LOAD)
    List<Client> findAllWithTransactions();

    @Query("select distinct c from Client c")
    @EntityGraph(value = "clientWithTransactionAndBook", type = EntityGraph.EntityGraphType.LOAD)
    List<Client> findAllWithTransactionsAndBook();
}
