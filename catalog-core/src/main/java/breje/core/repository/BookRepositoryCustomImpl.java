package breje.core.repository;

import breje.core.model.Book;
import breje.core.model.Transaction;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.jpa.HibernateEntityManager;
import org.hibernate.jpa.event.internal.core.HibernateEntityManagerEventListener;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.util.List;

public class BookRepositoryCustomImpl extends CustomRepositorySupport
{
    public List<Book> findAllWithTransactionsAndClientJPQL()
    {
        EntityManager entityManager = getEntityManager();
        Query query = entityManager.createQuery(
                "select distinct b from Book b left join fetch b.bookTransactions t left join t.client"
        );
        List<Book> books = query.getResultList();

        return books;
    }

//    public List<Book> findAllWithTransactionsAndClientCriteriaAPI()
//    {
//        EntityManager entityManager = getEntityManager();
//        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
//
//        CriteriaQuery<Book> query = criteriaBuilder.createQuery(Book.class);
//        query.distinct(Boolean.TRUE);
//        Root<Book> root = query.from(Book.class);
//
//        Fetch<Book, Transaction> bookTransactionFetch = root.fetch(Book_.bookTransactions, JoinType.LEFT);
//        bookTransactionFetch.fetch(Transaction_.client, JoinType.LEFT);
//
//        List<Book> books = entityManager.createQuery(query).getResultList();
//        )
//    }

    @Transactional
    public List<Book> findAllWithTransactionsAndClientSQL()
    {
        HibernateEntityManager hibernateEntityManager = getEntityManager().unwrap(HibernateEntityManager.class);
        Session session = hibernateEntityManager.getSession();

        org.hibernate.Query query = session.createSQLQuery("select distinct {b.*},{t.*},{c.*} from book b " +
                "left join transaction t on a._id=t.book__id " +
                "left join client c on t.client__id=c._id ").addEntity("b", Book.class)
                .addJoin("t", "b.bookTransactions")
                .addJoin("c", "t.client")
                .addEntity("b", Book.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        List<Book> books = query.getResultList();
        return books;
    }

    @Transactional
    public List<Book> findAllFromGivenYearJPQL(int year)
    {
        EntityManager entityManager = getEntityManager();
        Query query = entityManager.createQuery(
                "select distinct b from Book b left join fetch b.bookTransactions t left join t.client where b.year= :pyear"
        );
        query.setParameter("pyear", year);
        List<Book> books = query.getResultList();

        return books;
    }

//    @Transactional
//    public List<Book> findAllFromGivenYearCriteriaAPI(int year)
//    {
//        EntityManager entityManager = getEntityManager();
//        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
//
//        CriteriaQuery<Book> query = criteriaBuilder.createQuery(Book.class);
//        query.distinct(Boolean.TRUE);
//        Root<Book> root = query.from(Book.class);
//        query.where(criteriaBuilder.equals(root.get("year"), year));
//
//        Fetch<Book, Transaction> bookTransactionFetch = root.fetch(Book_.bookTransactions, JoinType.LEFT);
//        bookTransactionFetch.fetch(Transaction_.client, JoinType.LEFT);
//
//
//        List<Book> books = entityManager.createQuery(query).getResultList();
//    }


    @Transactional
    public List<Book> findAllFromGivenYearSQL(int year)
    {
        HibernateEntityManager hibernateEntityManager = getEntityManager().unwrap(HibernateEntityManager.class);
        Session session = hibernateEntityManager.getSession();

        org.hibernate.Query query = session.createSQLQuery("select distinct {b.*},{t.*},{c.*} from book b " +
                "left join transaction t on a._id=t.book__id " +
                "left join client c on t.client__id=c._id " +
                "where b.year = :paramYear")
                .setParameter(":paramYear", year)
                .addEntity("b", Book.class)
                .addJoin("t", "b.bookTransactions")
                .addJoin("c", "t.client")
                .addEntity("b", Book.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        List<Book> books = query.getResultList();
        return books;
    }
}
