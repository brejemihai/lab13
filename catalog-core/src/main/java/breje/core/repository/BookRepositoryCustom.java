package breje.core.repository;

import breje.core.model.Book;

import java.util.List;

public interface BookRepositoryCustom
{
    List<Book> findAllWithTransactionsAndClientJPQL();

    List<Book> findAllWithTransactionsAndClientCriteriaAPI();

    List<Book> findAllWithTransactionsAndClientSQL();

    List<Book> findAllFromGivenYearJPQL(int year);
    List<Book> findAllFromGivenYearCriteriaAPI(int year);
    List<Book> findAllFromGivenYearSQL(int year);
}
