package breje.core.repository;

import breje.core.model.Book;
import breje.core.model.Client;

import java.util.List;

public interface ClientRepositoryCustom
{
    List<Client> findAllWithTransactionsAndBookJPQL();

    List<Client> findAllWithTransactionsAndBookCriteriaAPI();

    List<Client> findAllWithTransactionsAndBookSQL();

    List<Client> findAllByGenderJPQL();

    List<Client> findAllByGenderCriteriaAPI();

    List<Client> findAllByGenderSQL();

}
