package breje.core.repository;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class CustomRepositorySupport {
    @PersistenceContext
    private EntityManager entityManager;

    public EntityManager getEntityManager()
    {
        return this.entityManager;
    }
}
