package breje.web.controller;

import breje.core.model.Client;
import breje.core.service.ClientService;
import breje.web.converter.ClientConverter;
import breje.web.dto.ClientDTO;
import breje.web.dto.ClientSpentDTO;
import breje.web.dto.ClientsDTO;
import breje.web.dto.ClientsSpentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("clients")
public class ClientController
{
    @Autowired
    private ClientService clientService;

    @Autowired
    private ClientConverter clientConverter;

    @RequestMapping(method = RequestMethod.GET)
    public ClientsDTO getUsers()
    {
        Iterable<Client> clienti = this.clientService.getAllClients();
        Set<ClientDTO> clients = StreamSupport.stream(clienti.spliterator(), false).map(clientConverter::convertModelToDto).collect(Collectors.toSet());
        return new ClientsDTO(clients);
    }

    @RequestMapping(path="{id}", method = RequestMethod.GET)
    public ClientDTO getClient(@PathVariable("id") Long id) throws Exception
    {
        Client theClient;
        if(this.clientService.getOneClient(id).isPresent())
        {
            theClient = this.clientService.getOneClient(id).get();
        }
        else
        {
            throw new Exception("NEM");
        }
        return this.clientConverter.convertModelToDto(theClient);
    }

    @RequestMapping(method = RequestMethod.POST)
    public void addUser(@RequestBody ClientDTO clientDTO) throws Exception
    {
        this.clientService.addClient(clientConverter.convertDtoToModel(clientDTO));
    }

    @RequestMapping(method = RequestMethod.PUT)
    public void updateUser(@RequestBody ClientDTO clientDTO) throws Exception
    {
        this.clientService.updateClient(clientConverter.convertDtoToModel(clientDTO));
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public void removeUser(@PathVariable("id") Long cnp) throws Exception
    {
        this.clientService.deleteClient(cnp);
    }

    @RequestMapping(path = "spent", method = RequestMethod.GET)
    public ClientsSpentDTO getTopClients() throws Exception
    {
//        List<Map.Entry<Client, Integer>> lista = this.transactionService.reportClientsBySpending();
//        List<ClientSpentDTO> innerList = new ArrayList<>();
//        lista.stream().forEach( x ->
//        {
//            Client cl = x.getKey();
//            Integer price = x.getValue();
//            innerList.add(new ClientSpentDTO(cl.getCNP(), cl.getYob(), cl.getGender(), price));
//        });
//        return new ClientsSpentDTO(innerList);
        return new ClientsSpentDTO();
    }
}
