package breje.web.controller;

import breje.core.model.Book;
import breje.core.model.Client;
import breje.core.service.BookService;
import breje.web.converter.BookConverter;
import breje.web.dto.BookDTO;
import breje.web.dto.BooksDTO;
import breje.web.dto.ClientDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("books")
public class BookController
{
    @Autowired
    private BookService bookService;

    @Autowired
    private BookConverter bookConverter;

    @RequestMapping(method = RequestMethod.GET)
    public BooksDTO getBooks()
    {
        Iterable<Book> books = this.bookService.getAllBooks();
        Set<BookDTO> bks = StreamSupport.stream(books.spliterator(), false).map(bookConverter::convertModelToDto).collect(Collectors.toSet());
        return new BooksDTO(bks);
    }

    @RequestMapping(path="{id}", method = RequestMethod.GET)
    public BookDTO getBook(@PathVariable("id") Long bid) throws Exception
    {
        Book theClient;
        if(this.bookService.getOneBook(bid).isPresent())
        {
            theClient = this.bookService.getOneBook(bid).get();
        }
        else
        {
            throw new Exception("NEM");
        }
        return this.bookConverter.convertModelToDto(theClient);
    }

    @RequestMapping(method = RequestMethod.POST)
    public void addBook(@RequestBody BookDTO bookDTO) throws Exception
    {
        this.bookService.addBook(bookConverter.convertDtoToModel(bookDTO));
    }

    @RequestMapping(method = RequestMethod.PUT)
    public void updateBook(@RequestBody BookDTO bookDTO) throws Exception
    {
        this.bookService.updateBook(bookConverter.convertDtoToModel(bookDTO));
    }

    @RequestMapping(path="/{id}", method = RequestMethod.DELETE)
    public void deleteBook(@PathVariable("id") Long id) throws Exception
    {
        this.bookService.deleteBook(id);
    }

    @RequestMapping(path="/names/{name}", method = RequestMethod.GET)
    public BooksDTO filterByName(@PathVariable("name") String name)
    {
        Iterable<Book> filtered = this.bookService.getBookContainingSubstring(name);
        Set<BookDTO> bks = StreamSupport.stream(filtered.spliterator(), false).map(bookConverter::convertModelToDto).collect(Collectors.toSet());
        return new BooksDTO(bks);
    }
}
