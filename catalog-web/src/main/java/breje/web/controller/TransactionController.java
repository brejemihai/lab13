package breje.web.controller;

import breje.core.model.Client;
import breje.core.model.Transaction;
import breje.core.service.BookService;
import breje.core.service.ClientService;
import breje.web.converter.TransactionConverter;
import breje.web.dto.TransactionDTO;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("manage")
public class TransactionController
{
    @Autowired
    private ClientService clientService;

    @Autowired
    private BookService bookService;

    @Autowired
    private TransactionConverter transactionConverter;


    @RequestMapping(method = RequestMethod.GET)
    public List<TransactionDTO> getTransactions()
    {
        Iterable<Client> clienti = this.clientService.getAllClients();
        List<Transaction> toate = new ArrayList<>();

        clienti.forEach(x-> toate.addAll(x.getTransactions()));

        return toate.stream().map(transactionConverter::convertModelToDto).collect(Collectors.toList());
    }

    @Transactional
    @RequestMapping(path="client={cid}&book={bid}", method = RequestMethod.POST)
    public void addTransaction(@PathVariable("cid") Long cid, @PathVariable("bid") Long bid, @RequestBody TransactionDTO transactionDTO) throws Exception
    {
        try
        {
            transactionDTO.setClientID(cid);
            transactionDTO.setBookID(bid);
        }
        catch (Exception e)
        {
            throw new Exception("CSF");
        }

        Transaction newTrans = transactionConverter.convertDtoToModel(transactionDTO);
        this.clientService.addTransaction(transactionDTO.getClientID(), transactionDTO.getBookID(), transactionDTO.getPrice(), transactionDTO.getTransactionDate());
    }

    @RequestMapping(path="client={cid}&book={bid}", method = RequestMethod.DELETE)
    public void deleteTransaction(@PathVariable("cid") Long cid, @PathVariable("bid") Long bid) throws Exception
    {
        if(clientService.getOneClient(cid).isPresent() && bookService.getOneBook(bid).isPresent())
        {
            this.clientService.removeTransaction(clientService.getOneClient(cid).get(), bookService.getOneBook(bid).get());
        }
    }

    @RequestMapping(path="client={cid}&book={bid}", method = RequestMethod.PUT)
    public void updateTransaction(@PathVariable("cid") Long cid, @PathVariable("bid") Long bid, @RequestBody TransactionDTO transactionDTO) throws Exception
    {
        try
        {
            transactionDTO.setClientID(cid);
            transactionDTO.setBookID(bid);
        }
        catch (Exception e)
        {
            throw new Exception("CSF");
        }
        this.clientService.updateTransaction(transactionDTO.getClientID(), transactionDTO.getBookID(), transactionDTO.getPrice(), transactionDTO.getTransactionDate());
    }

    @RequestMapping(path="client={id}", method = RequestMethod.GET)
    public List<TransactionDTO> getTransactionForClient(@PathVariable("id") Long cid) throws Exception
    {
        Iterable<Transaction> transactions = this.clientService.getAllTransactionsForClient(cid);
        return StreamSupport.stream(transactions.spliterator(), false).map(transactionConverter::convertModelToDto).collect(Collectors.toList());
    }

    @RequestMapping(path="book={id}", method = RequestMethod.GET)
    public List<TransactionDTO> getTransactionForBook(@PathVariable("id") Long bid) throws Exception
    {
        Iterable<Transaction> transactions = this.bookService.getAllTransactionsForBook(bid);
        return StreamSupport.stream(transactions.spliterator(), false).map(transactionConverter::convertModelToDto).collect(Collectors.toList());
    }

    @RequestMapping(path="price={dat}", method = RequestMethod.GET)
    public List<TransactionDTO> getAllTransactionsWithPrice(@PathVariable("dat") int pret) throws Exception
    {
        Iterable<Transaction> transactions = this.clientService.getAllTransactionWithPrice(pret);
        return StreamSupport.stream(transactions.spliterator(), false).map(transactionConverter::convertModelToDto).collect(Collectors.toList());
    }
//    @Autowired
//    private TransactionService transactionService;
//    @Autowired
//    private TransactionConverter transactionConverter;
//
//    @Autowired
//    private ClientService clientService;
//    @Autowired
//    private BookService bookService;
//
//    @RequestMapping(method = RequestMethod.GET)
//    public List<TransactionDTO> getTransactions()
//    {
//        Iterable<Transaction> trans = this.transactionService.getAllTransactions();
//        return StreamSupport.stream(trans.spliterator(), false).map(transactionConverter::convertModelToDto).collect(Collectors.toList());
//    }
//
//    @RequestMapping(path="client={cid}&book={bid}", method = RequestMethod.POST)
//    public void addTransaction(@PathVariable("cid") Long cid, @PathVariable("bid") Long bid, @RequestBody TransactionDTO transactionDTO) throws Exception
//    {
//        try
//        {
//            transactionDTO.setBook(this.bookService.getOneBook(bid).get());
//            transactionDTO.setClient(this.clientService.getOneClient(cid).get());
//        }
//        catch (Exception e)
//        {
//            throw new Exception("CSF");
//        }
//
//        Transaction newTrans = transactionConverter.convertDtoToModel(transactionDTO);
//        this.transactionService.addTransaction(newTrans);
//    }
//
//    @RequestMapping(path="client={cid}&book={bid}", method = RequestMethod.DELETE)
//    public void deleteTransaction(@PathVariable("cid") Long cid, @PathVariable("bid") Long bid) throws Exception
//    {
//        this.transactionService.deleteTransaction(cid, bid);
//    }
//
//    @RequestMapping(path="client={cid}&book={bid}", method = RequestMethod.PUT)
//    public void updateTransaction(@PathVariable("cid") Long cid, @PathVariable("bid") Long bid, @RequestBody TransactionDTO transactionDTO) throws Exception
//    {
//        try
//        {
//            transactionDTO.setBook(this.bookService.getOneBook(bid).get());
//            transactionDTO.setClient(this.clientService.getOneClient(cid).get());
//        }
//        catch (Exception e)
//        {
//            throw new Exception("CSF");
//        }
//        Transaction newTrans = transactionConverter.convertDtoToModel(transactionDTO);
//        this.transactionService.updateTransaction(newTrans);
//    }
//
//    @RequestMapping(path="client={id}", method = RequestMethod.GET)
//    public List<TransactionDTO> getTransactionForClient(@PathVariable("id") Long cid) throws Exception
//    {
//        Iterable<Transaction> transactions = this.transactionService.getAllTransactionForClient(cid);
//        return StreamSupport.stream(transactions.spliterator(), false).map(transactionConverter::convertModelToDto).collect(Collectors.toList());
//    }
//
//    @RequestMapping(path="book={id}", method = RequestMethod.GET)
//    public List<TransactionDTO> getTransactionForBook(@PathVariable("id") Long bid) throws Exception
//    {
//        Iterable<Transaction> transactions = this.transactionService.getAllTransactionForClient(bid);
//        return StreamSupport.stream(transactions.spliterator(), false).map(transactionConverter::convertModelToDto).collect(Collectors.toList());
//    }
//
//    @RequestMapping(path="price={dat}", method = RequestMethod.GET)
//    public List<TransactionDTO> getAllTransactionsWithPrice(@PathVariable("dat") int pret) throws Exception
//    {
//        Iterable<Transaction> transactions = this.transactionService.getAllTransactionWithPrice(pret);
//        return StreamSupport.stream(transactions.spliterator(), false).map(transactionConverter::convertModelToDto).collect(Collectors.toList());
//    }

}
