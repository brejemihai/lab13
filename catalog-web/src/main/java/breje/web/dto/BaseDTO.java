package breje.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

public class BaseDTO implements Serializable
{
    @JsonProperty
    private Long id;

    public BaseDTO()
    {

    }

    public BaseDTO(Long id)
    {
        this.id = id;
    }

    public Long getID()
    {
        return this.id;
    }

    public void setID(Long id)
    {
        this.id = id;
    }

}
