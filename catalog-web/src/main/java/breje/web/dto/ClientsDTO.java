package breje.web.dto;


import java.util.Set;

public class ClientsDTO
{
    private Set<ClientDTO> clients;

    public ClientsDTO()
    {

    }

    public ClientsDTO(Set<ClientDTO> set)
    {
        this.clients = set;
    }

    public void setCollection(Set<ClientDTO> set )
    {
        this.clients = set;
    }

    public Set<ClientDTO> getClients()
    {
        return this.clients;
    }
}
