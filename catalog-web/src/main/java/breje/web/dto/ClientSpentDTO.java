package breje.web.dto;

import breje.core.model.Client;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class ClientSpentDTO
{
    @JsonProperty
    private long cnp;

    @JsonProperty
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date yob;

    @JsonProperty
    private String gender;

    @JsonFormat
    private Integer price;

    public ClientSpentDTO()
    {

    }

    public ClientSpentDTO(long cnp, Date yob, String gender, Integer price)
    {
        this.cnp = cnp;
        this.yob = yob;
        this.gender = gender;
        this.price = price;
    }

    public long getCnp() {
        return cnp;
    }

    public void setCnp(long cnp) {
        this.cnp = cnp;
    }

    public Date getYob() {
        return yob;
    }

    public void setYob(Date yob) {
        this.yob = yob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
