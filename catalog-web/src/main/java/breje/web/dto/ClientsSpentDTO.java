package breje.web.dto;

import java.util.List;

public class ClientsSpentDTO
{
    private List<ClientSpentDTO> set;

    public ClientsSpentDTO()
    {

    }

    public ClientsSpentDTO(List<ClientSpentDTO> setu)
    {
        this.set = setu;
    }

    public List<ClientSpentDTO> getSet() {
        return set;
    }

    public void setSet(List<ClientSpentDTO> set) {
        this.set = set;
    }
}
