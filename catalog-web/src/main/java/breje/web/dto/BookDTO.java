package breje.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;

public class BookDTO extends BaseDTO
{
    @JsonProperty
    private long BID;

    @JsonProperty
    private String author;

    @JsonProperty
    private int year;

    public BookDTO()
    {}

    public BookDTO(long bid, String author, int year)
    {
        this.BID = bid;
        this.author = author;
        this.year = year;
    }

    public long getBID()
    {
        return BID;
    }

    public void setBID(long BID)
    {
        this.BID = BID;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public int getYear()
    {
        return year;
    }

    public void setYear(int year)
    {
        this.year = year;
    }

    @Override
    public String toString()
    {
        return "Book: " +
                " BID=" + BID +
                ", author=" + author  +
                ", year=" + year;
    }
}
