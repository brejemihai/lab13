package breje.web.dto;


import java.util.Set;

public class BooksDTO
{
    private Set<BookDTO> books;

    public BooksDTO()
    {

    }
    public BooksDTO(Set<BookDTO> set)
    {
        this.books = set;
    }

    public Set<BookDTO> getBooks()
    {
        return this.books;
    }

    public void setBooks(Set<BookDTO> bookDTOS)
    {
        this.books = bookDTOS;
    }
}
