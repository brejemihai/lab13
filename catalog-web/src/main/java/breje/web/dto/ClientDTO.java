package breje.web.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import java.util.Date;

public class ClientDTO extends BaseDTO
{
    @JsonProperty
    private long cnp;

    @JsonProperty
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date yob;

    @JsonProperty
    private String gender;

    public ClientDTO()
    {

    }
//pattern="^((([1-2][0-9])|(3[0-1]))|([1-9]))-((1[0-2])|([1-9]))-[0-9]{4}$"
    public long getCNP()
    {
        return cnp;
    }

    public void setCNP(long CNP)
    {
        this.cnp = CNP;
    }

    public Date getYob()
    {
        return yob;
    }

    public void setYob(Date yob)
    {
        this.yob = yob;
    }

    public String getGender()
    {
        return gender;
    }

    public void setGender(String gender)
    {
        this.gender = gender;
    }

    public ClientDTO(long CNP, Date yob, String gender)
    {
        this.cnp = CNP;
        this.yob = yob;
        this.gender = gender;
    }

    public String toString()
    {
        return "Client { " + "ID: " +  String.valueOf(cnp) + " Year of birth: " + yob.toString() + " Gender: " + gender + " } " + super.toString();
    }
}
