package breje.web.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import java.util.Date;

public class TransactionDTO extends BaseDTO
{
    @JsonProperty
    private long clientID;

    @JsonProperty
    private long bookID;

    @JsonProperty
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date transactionDate;

    @JsonProperty
    private int price;

    public TransactionDTO()
    {
    }

    public TransactionDTO(Date transactionDate, int price)
    {
        this.transactionDate = transactionDate;
        this.price = price;
    }

    public long getClientID()
    {
        return clientID;
    }

    public void setClientID(long clientID)
    {
        this.clientID = clientID;
    }

    public long getBookID()
    {
        return bookID;
    }

    public void setBookID(long bookID)
    {
        this.bookID = bookID;
    }

    public Date getTransactionDate()
    {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate)
    {
        this.transactionDate = transactionDate;
    }

    public int getPrice()
    {
        return price;
    }

    public void setPrice(int price)
    {
        this.price = price;
    }

    @Override
    public String toString() {
        return "\nTransaction " + this.getID() + " from date: " + transactionDate + ", client: " +
                clientID + ", book: " + bookID + " bought for: " + this.price + " " + super.toString();
    }
}

