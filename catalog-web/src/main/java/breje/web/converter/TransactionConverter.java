package breje.web.converter;

import breje.core.model.Transaction;
import breje.web.dto.TransactionDTO;
import org.springframework.stereotype.Component;

@Component
public class TransactionConverter
{
    public Transaction convertDtoToModel(TransactionDTO dto)
    {
        Transaction newTrans = new Transaction();
        return newTrans;
    }

    public TransactionDTO convertModelToDto(Transaction transaction)
    {
        TransactionDTO newTrans = new TransactionDTO(transaction.getTransactionDate(), transaction.getPrice());
        newTrans.setBookID(transaction.getBook().getID());
        newTrans.setClientID(transaction.getClient().getID());
        return newTrans;
    }
}
