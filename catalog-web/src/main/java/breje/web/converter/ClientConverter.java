package breje.web.converter;

import breje.core.model.Client;
import breje.web.dto.ClientDTO;
import org.springframework.stereotype.Component;

@Component
public class ClientConverter implements Converter<Client, ClientDTO>
{
    @Override
    public Client convertDtoToModel(ClientDTO dto)
    {
        Client newClient = new Client(dto.getCNP(), dto.getYob(), dto.getGender());
        newClient.setID(dto.getID());
        return newClient;
    }

    @Override
    public ClientDTO convertModelToDto(Client client)
    {
        ClientDTO clientDTO = new ClientDTO(client.getCNP(), client.getYob(), client.getGender());
        clientDTO.setID(client.getID());
        return clientDTO;
    }
}
