package breje.web.converter;

import breje.core.model.Book;
import breje.web.dto.BookDTO;
import org.springframework.stereotype.Component;

@Component
public class BookConverter implements Converter<Book, BookDTO>
{
    @Override
    public Book convertDtoToModel(BookDTO dto)
    {
        Book newBook = new Book(dto.getBID(), dto.getAuthor(), dto.getYear());
        newBook.setID(dto.getBID());
        return newBook;
    }

    @Override
    public BookDTO convertModelToDto(Book book)
    {
        BookDTO newBookDTO = new BookDTO(book.getBID(), book.getAuthor(), book.getYear());
        newBookDTO.setID(book.getID());
        return newBookDTO;
    }
}
