package breje.web.converter;

import breje.core.model.BaseEntity;
import breje.web.dto.BaseDTO;

/**
 * Created by radu.
 */

public interface Converter<Model extends BaseEntity<Long>, Dto extends BaseDTO> {

    Model convertDtoToModel(Dto dto);

    Dto convertModelToDto(Model model);

}


